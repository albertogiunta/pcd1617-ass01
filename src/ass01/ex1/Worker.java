package ass01.ex1;

public class Worker extends Thread {

    private int w, hstart, nrows, nIterMax, constW, constH;
    private int image[];
    private Complex center;
    private double delta;

    public Worker(int w, int hstart, int nrows, int[] image, Complex center, double delta, int nIterMax, int constW, int constH) {
        this.w = w;
        this.hstart = hstart;
        this.nrows = nrows;
        this.image = image;
        this.center = center;
        this.delta = delta;
        this.nIterMax = nIterMax;
        this.constH = constH;
        this.constW = constW;
    }

    public void run() {
        log("started " + w + " " + hstart);
        for (int x = 0; x < w; x++){
            for (int y = hstart; y < (hstart + nrows); y++){
                Complex c = getPoint(x,y);
                double level = computeColor(c, nIterMax);
                int color = (int)(level*255);
                image[y*w+x] = color + (color << 8) + (color << 16);
            }
        }

        log("done ");
    }

    private void log(String msg){
        System.out.println("[WORKER] "+msg);
    }

    /**
     * Basic Mandelbrot set algorithm
     *
     * @param c
     * @param maxIteration
     * @return
     */
    private double computeColor(Complex c, int maxIteration){
        int iteration = 0;
        Complex z = new Complex(0,0);

		/*
		 * Repeatedly compute z := z^2 + c
		 * until either the point is out
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 *
		 */
        while ( z.absFast() <= 2 &&  iteration < maxIteration ){
            z = z.times(z).plus(c);
            iteration++;
        }
        if ( iteration == maxIteration ){
			  /* the point belongs to the set */
            return 0;
        } else {
			  /* the point does not belong to the set => distance */
            return 1.0-((double)iteration)/maxIteration;
        }
    }

    /**
     * This method returns the point in the complex plane
     * corresponding to the specified element/pixel in the image
     *
     * @param x x coordinate in the image
     * @param y y coordinate in the image
     * @return the corresponding complex point
     */
    private Complex getPoint(int x, int y){
        return new Complex((x - constW*0.5)*delta + center.re(), center.im() - (y - constH*0.5)*delta);
    }

}
