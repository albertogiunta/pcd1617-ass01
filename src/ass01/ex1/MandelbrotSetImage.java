package ass01.ex1;

public interface MandelbrotSetImage {

	
	/**
	 * 
	 * Compute the image with the specified level of detail
	 * 
	 * See https://en.wikipedia.org/wiki/Mandelbrot_set
	 * 
	 * @param nIterMax number of iteration representing the level of detail
	 */
	void compute(int nIterMax);
	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	Complex getPoint(int x, int y);
	
	/**
	 * Get the height of the image
	 * @return
	 */
	int getHeight();
	

	/**
	 * Get the width of the image
	 * @return
	 */
	int getWidth();
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	int[] getImage();
}
