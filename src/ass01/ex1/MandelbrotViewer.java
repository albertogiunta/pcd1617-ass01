package ass01.ex1;

/**
 * Simple Mandelbrot Set Viewer
 *
 * @author aricci
 */
public class MandelbrotViewer {
    public static void main(String[] args) throws Exception {

		/* size of the mandelbrot set in pixel */
        int width  = 4000;
        int height = 4000;
		
		/* number of iteration */
        int nIter = 500;

		/* region to be represented: center and radius */
        Complex c0   = new Complex(-0.75, 0);
        double  rad0 = 2;


        Complex c1   = new Complex(-0.75, 0.1);
        double  rad1 = 0.02;

        Complex c2   = new Complex(0.7485, 0.0505);
        double  rad2 = 0.000002;

        Complex c3   = new Complex(0.254, 0);
        double  rad3 = 0.001;

		
		/* creating the set */
		MandelbrotSetImage set1 = new MandelbrotSetImageImpl(width,height, c0, rad0);
		MandelbrotSetImage set2 = new MandelbrotSetImageConcurImpl(width,height, c0, rad0);

//		MandelbrotSetImage set1 = new MandelbrotSetImageImpl(width,height, c1, rad1);
//		MandelbrotSetImage set2 = new MandelbrotSetImageConcurImpl(width,height, c1, rad1);

//		MandelbrotSetImage set1 = new MandelbrotSetImageImpl(width,height, c2, rad2);
//		MandelbrotSetImage set2 = new MandelbrotSetImageConcurImpl(width,height, c2, rad2);

//		MandelbrotSetImage set1 = new MandelbrotSetImageImpl(width,height, c3, rad3);
//      MandelbrotSetImage set2 = new MandelbrotSetImageConcurImpl(width, height, c3, rad3);

        System.out.println("Computing sequential...");
        StopWatch cron = new StopWatch();
        cron.start();
        set1.compute(nIter);
        cron.stop();
        long done1 = cron.getTime();

        System.out.println("Computing parallel...");
        cron = new StopWatch();
        cron.start();
        set2.compute(nIter);
        cron.stop();
        long done2 = cron.getTime();


        float speedup = (float)done1/done2;
        System.out.println("sequenziale - " + done1 + " ms");
        System.out.println("parallelo   - " + done2 + " ms");
        System.out.println("speedup     - " + speedup + "");

		/* showing the image */
        MandelbrotView view = new MandelbrotView(set2, 800, 600);
        view.setVisible(true);


    }

}
