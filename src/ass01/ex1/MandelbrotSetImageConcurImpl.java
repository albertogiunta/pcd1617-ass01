package ass01.ex1;

/**
 * Class for computing an image of a region
 * of the Mandelbrot set.
 *
 * @author aricci
 */
public class MandelbrotSetImageConcurImpl implements MandelbrotSetImage {

    private Worker[] workers;
    private Thread[] threads;
    private int      w, h;
    private int     image[];
    private Complex center;
    private double  delta;


    /**
     * Creating an empty Mandelbrot set image.
     *
     * @param w width in pixels
     * @param h height in pixels
     * @param c center of the Mandelbrot set to be represented
     * @param radius radius of the Mandelbrot set to be represented
     */
    public MandelbrotSetImageConcurImpl(int w, int h, Complex c, double radius) {
        this.w = w;
        this.h = h;
        image = new int[w * h];
        center = c;
        delta = radius / (w * 0.5);
        workers = new Worker[Runtime.getRuntime().availableProcessors()+1];
        threads = new Thread[Runtime.getRuntime().availableProcessors()+1];
    }

    /**
     * Compute the image with the specified level of detail
     *
     * See https://en.wikipedia.org/wiki/Mandelbrot_set
     *
     * @param nIterMax number of iteration representing the level of detail
     */
    @SuppressWarnings("Duplicates")
    public void compute(int nIterMax) {

		/*
         * for each pixel of the image
		 * - get the corresponding point on the complex plan
		 * - verify if the point either belongs or not to the Mandelbrot set
		 * -- yes => black color
		 * -- no => level of gray, depending on the distance from the set
		 */

//      VERSIONE OTTIMIZZATA (ottenimento dei chunk dinamico)
        Counter rowProducer = new Counter();
        try {
            for (int i = 0; i < threads.length; i++) {
                threads[i] = new Thread(() -> {
                    while (rowProducer.getCount() < h) {
                        int value;
                        synchronized (rowProducer){
                            rowProducer.inc();
                            value = rowProducer.getCount();
                            if (!(value < h)) break;
                        }
                        for (int col = 0; col < w; col++) {
                            Complex c = getPoint(col, value);
                            double level = computeColor(c,nIterMax);
                            int color = (int)(level*255);
                            image[value*w+col] = color + (color << 8)+ (color << 16);
                        }
                    }
                });
                threads[i].start();
            }
            for (Thread thread : threads) {
                thread.join();
            }
            System.out.println(rowProducer.getCount());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


//      VERSIONE BANALE (divisione dei chunk in base al numero dei thread)

//        int         nrows    = h / workers.length;
//        int         irow     = 0;
//        try {
//            for (int i = 0; i < workers.length-1; i++){
//                workers[i] = new Worker(w, irow, nrows, image, center, delta, nIterMax, w, h);
//                workers[i].start();
//                irow += nrows;
//            }
//            workers[workers.length-1] = new Worker(w, irow, nrows, image, center, delta, nIterMax, w, h);
//            workers[workers.length-1].start();
//            for (Thread w: workers){
//                w.join();
//            }
//        } catch (Exception e){
//          e.printStackTrace();
//        }
    }

    /**
     * This method returns the point in the complex plane
     * corresponding to the specified element/pixel in the image
     *
     * @param x x coordinate in the image
     * @param y y coordinate in the image
     * @return the corresponding complex point
     */

    public Complex getPoint(int x, int y) {
        return new Complex((x - w * 0.5) * delta + center.re(), center.im() - (y - h * 0.5) * delta);
    }

    /**
     * Basic Mandelbrot set algorithm
     */
    private double computeColor(Complex c, int maxIteration) {
        int     iteration = 0;
        Complex z         = new Complex(0, 0);

		/*
         * Repeatedly compute z := z^2 + c
		 * until either the point is out
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 *
		 */
        while (z.absFast() <= 2 && iteration < maxIteration) {
            z = z.times(z).plus(c);
            iteration++;
        }
        if (iteration == maxIteration) {
              /* the point belongs to the set */
            return 0;
        } else {
              /* the point does not belong to the set => distance */
            return 1.0 - ((double) iteration) / maxIteration;
        }
    }

    /**
     * Get the height of the image
     */
    public int getHeight() {
        return h;
    }

    /**
     * Get the width of the image
     */
    public int getWidth() {
        return w;
    }

    /**
     * Get the image as an array of int, organized per rows
     * (compatible with the BufferedImage style)
     */
    public int[] getImage() {
        return image;
    }

    static class Counter {
        private int count;

        public Counter() {
            count = 0;
        }

        public synchronized void inc() {
            count++;
        }

        public synchronized int getCount() {
            return count;
        }
    }


}

