package ass01.ex2;

public class MyModel {

    private int counter;

    public MyModel() {
        counter = 0;
    }

    public synchronized int getCounter() {
        return counter;
    }

    public synchronized void incrementCounter() {
        counter++;
    }
}
