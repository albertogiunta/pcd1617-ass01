package ass01.ex2;

import java.util.concurrent.Semaphore;

public class Pinger extends Worker {

    private Semaphore pingerMutex, pongerMutex, viewerMutex, permissionToStartMutex, canOthersDieMutex;

    public Pinger(Semaphore pingerMutex, Semaphore pongerMutex, Semaphore viewerMutex, Semaphore permissionToStartMutex, Semaphore canOthersDieMutex, MyModel model) {
        super("PINGER", model);
        this.pingerMutex = pingerMutex;
        this.pongerMutex = pongerMutex;
        this.viewerMutex = viewerMutex;
        this.permissionToStartMutex = permissionToStartMutex;
        this.canOthersDieMutex = canOthersDieMutex;
    }

    public void run() {
        try {
            while (run) {
                pingerMutex.acquire();
                permissionToStartMutex.acquire();
                println("\t\tping!");
                getModel().incrementCounter();
                viewerMutex.release();
                pongerMutex.release();
            }
            println("\t\t\t\t\t\tGoodbye");
            canOthersDieMutex.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

