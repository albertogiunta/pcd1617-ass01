package ass01.ex2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

class MyView extends JFrame implements ActionListener {

    private MyController controller;
    private JButton      button;

    public MyView(MyController controller) {
        super("My View");

        this.controller = controller;

        setSize(400, 60);
        setResizable(false);

        button = new JButton("START");
        button.addActionListener(this);

        JPanel panel = new JPanel();
        panel.add(button);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.NORTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                System.exit(-1);
            }
        });
    }

    public void actionPerformed(ActionEvent ev) {
        controller.processEvent(button.getText());
        button.setText("STOP");
    }
}
