package ass01.ex2;

import java.util.concurrent.Semaphore;

public class Ponger extends Worker {

    private Semaphore pingerMutex, pongerMutex, viewerMutex, permissionToStartMutex, canOthersDieMutex;

    public Ponger(Semaphore pingerMutex, Semaphore pongerMutex, Semaphore viewerMutex, Semaphore permissionToStartMutex, Semaphore canOthersDieMutex, MyModel model) {
        super("PONGER", model);
        this.pingerMutex = pingerMutex;
        this.pongerMutex = pongerMutex;
        this.viewerMutex = viewerMutex;
        this.permissionToStartMutex = permissionToStartMutex;
        this.canOthersDieMutex = canOthersDieMutex;
    }

    public void run() {
        try {
            while (run) {
                pongerMutex.acquire();
                permissionToStartMutex.acquire();
                println("\t\t\t\tPONG!");
                getModel().incrementCounter();
                viewerMutex.release();
                pingerMutex.release();
            }
            println("\t\t\t\t\t\tGoodbye");
            canOthersDieMutex.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
