package ass01.ex2;

import java.util.concurrent.Semaphore;

public class Viewer extends Worker {

    private Semaphore viewerMutex, permissionToStartMutex;

    public Viewer(Semaphore viewerMutex, Semaphore permissionToStartMutex, MyModel model) {
        super("VIEWER", model);
        this.viewerMutex = viewerMutex;
        this.permissionToStartMutex = permissionToStartMutex;
    }

    @Override
    public void run() {
        try {
            int prev = -1;
            while (run) {
                viewerMutex.acquire();
                if (getModel().getCounter() > prev) {
                    println(Integer.toString(getModel().getCounter()));
                    prev = getModel().getCounter();
                }
                permissionToStartMutex.release();
            }
            println("\t\t\t\t\t\tGoodbye");
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
