package ass01.ex2;

public class TestGUI {
  static public void main(String[] args){
	  
	MyModel      model      = new MyModel();
	MyController controller = new MyController(model);
    MyView       view       = new MyView(controller);
    view.setVisible(true);
  }
}
