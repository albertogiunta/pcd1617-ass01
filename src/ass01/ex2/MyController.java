package ass01.ex2;


import java.util.concurrent.Semaphore;

public class MyController {

    private MyModel model;
    private Worker  pinger, ponger, viewer;
    private Semaphore pingerMutex, pongerMutex, viewerMutex, permissionToStartMutex, canOthersDieMutex;

    public MyController(MyModel model) {
        this.model = model;
        pingerMutex = new Semaphore(1);
        pongerMutex = new Semaphore(0);
        viewerMutex = new Semaphore(0);
        permissionToStartMutex = new Semaphore(1);
        canOthersDieMutex = new Semaphore(0);
        pinger = new Pinger(pingerMutex, pongerMutex, viewerMutex, permissionToStartMutex, canOthersDieMutex, model);
        ponger = new Ponger(pingerMutex, pongerMutex, viewerMutex, permissionToStartMutex, canOthersDieMutex, model);
        viewer = new Viewer(viewerMutex, permissionToStartMutex, model);
    }

    public void processEvent(String event) {
        if (event.equalsIgnoreCase("start")) {
            pinger.start();
            ponger.start();
            viewer.start();
        } else if (event.equalsIgnoreCase("stop")) {
            try {
                pinger.die();
                ponger.die();
                canOthersDieMutex.acquire();
                canOthersDieMutex.acquire();
                viewerMutex.release();
                viewer.die();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
