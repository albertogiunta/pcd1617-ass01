package ass01.ex2;

public abstract class Worker extends Thread {

    private   MyModel model;
    protected boolean run;

    public Worker(String name, MyModel model) {
        super(name);
        this.run = true;
        this.model = model;
    }

    public MyModel getModel() {
        return model;
    }

    protected void print(String msg) {
        synchronized (System.out) {
            System.out.print(msg);
        }
    }

    protected void println(String msg) {
        synchronized (System.out) {
            System.out.println("[" + getName() + "] " + msg);
        }
    }

    public void die() {
        run = false;
    }
}
